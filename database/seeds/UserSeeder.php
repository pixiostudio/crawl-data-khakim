<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Son Nguyen",
            'email' => "sonnt@pixiostudio.com",
            'password' => Hash::make("@ts01224115809"),
        ]);
    }
}
