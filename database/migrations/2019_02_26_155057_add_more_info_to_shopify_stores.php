<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreInfoToShopifyStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_stores', function (Blueprint $table) {
            //
            $table->string('currency')->default('USD');
            $table->string('category')->default('Apparel & Accessories > Clothing');
            $table->string('age_group')->default('adult');
            $table->string('gender')->nullable('male');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_stores', function (Blueprint $table) {
            //
            $table->dropColumn('currency');
            $table->dropColumn('category');
            $table->dropColumn('age_group');
            $table->dropColumn('gender');
        });
    }
}
