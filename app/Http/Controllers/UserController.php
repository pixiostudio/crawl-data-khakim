<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as Input;
use App\User;
use Auth;
use Hash;

use App\Helpers\SendMail;
use App\Mail\AgentForgotPassword;

class UserController extends Controller
{
    protected function isValidEmail($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function login(Request $request) {
        if (!$this->isValidEmail($request->input('email'))) {
            if ($request->email)
                return redirect('/login')->with('error', 'Email is invalid');
            else
                return redirect('/login')->with('error', '');
        }
        $userdata = array(
            'email'     => $request->input('email'),
            'password'  => $request->input('password')
        );
        Auth::attempt($userdata);
        $user = Auth::user();

        if ($user && $user->is_book_tour_user == 1) {
            return redirect('/login-user');
        }
        if (!$user) {
            return redirect('/login')->with('error', 'Invalid Login information');
        }
        return redirect('/admin/add-merchant-account')->with('message', 'Welcome to Google Shopping Admin');
    }

    public function register(Request $request) {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if (!$this->isValidEmail($user->email)) {
            return redirect('/register')->with('error', 'Email is invalid');
        } else {
            $userEmail = User::where('email', $user->email)->first();
            if ($userEmail) return redirect('/register')->with('error', 'Email is already used');
        }
        $user->password = bcrypt($request->input('password'));
        $user->save();
        $userdata = array(
            'email'     => $request->input('email'),
            'password'  => $request->input('password')
        );
        Auth::attempt($userdata);
        return redirect('/admin')->with('message', 'Welcome to Google Shopping Admin');
    }

    public function forgotPassword() {
        return view('auth.reset-password');
    }
    public function forgotPasswordHandle(Request $request) {
        $email = $request->input('email');
        $user = User::where('email', $email)
                        ->where('is_book_tour_user', 0)
                        ->first();
        if ($user) {
          // send email reset password with link to user
          $resetPassToken = substr(md5(date("Y-m-d h:i:sa").$email), 0, rand());
          $resetPassUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                  "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .'/agent/reset-password/'.$resetPassToken;
          $user->reset_password_token = $resetPassUrl;
          $user->save();

          //Send email to user
          SendMail::sendToCustomEmailAddress(
              'sg3plcpnl0013.prod.sin3.secureserver.net',
              587,
              'partners@triptour.vn',
              'triptour123',
              $user->email,
              new AgentForgotPassword($user)
          );
          return redirect('/agent/forgot-password')->with('error', 'Email reset password đang được gửi đến quý khách. Vui lòng check mail và thực hiện theo hướng dẫn');
        }
        return redirect('/agent/forgot-password')->with('error', 'Email không tồn tại!');
    }
    public function agentResetPassword($id) {
        return view('auth.agent-reset-password')
                ->with('id', $id);
    }
    public function resetPassword($id) {
        $user = User::where('reset_password_token', 'LIKE' ,'%'.$id.'%')
                      ->where([
                              'is_confirmed_reset_password' => 0,
                              'is_book_tour_user' => 0
                          ])
                      ->first();
        if ($user) {
          $user->is_confirmed_reset_password = 1;
          $user->save();
          // Auth::loginUsingId($user->id);
          return redirect('/agent/reset-password/agent/'.$id)
                  ->with('success_message', 'Vui lòng đặt lại mật khẩu');
        } else {
          abort(404);
        }
    }
    public function resetPasswordHandle(Request $request, $id) {
      $password = $request->input('password');
      $user = User::where('reset_password_token', 'LIKE' ,'%'.$id.'%')
                    ->where([
                            'is_confirmed_reset_password' => 1,
                            'is_book_tour_user' => 0
                            ])->first();
      if ($user) {
        $user->password = bcrypt($password);
        $user->reset_password_token = '';
        $user->is_confirmed_reset_password = 0;
        $user->logged_in = 1;
        $user->save();
        $userdata = array(
            'email'     => $user->email,
            'password'  => $password
        );
        Auth::attempt($userdata);
      	return redirect('/admin')->with(['success_message' => 'Thay đổi mật khẩu thành công']);
      } else {
        abort(404);
      }
    }
}
