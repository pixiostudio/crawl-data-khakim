<?php

namespace App\Http\Controllers;

use Google_Client;
use Google_Service_ShoppingContent;
use App\GoogleAccount;

use Illuminate\Http\Request;

class GoogleAccountController extends Controller
{
    public function index() {
      $googleAccounts = GoogleAccount::all();
      foreach($googleAccounts as $googleAccount) {
          if ($googleAccount->success_connect) {
            $client = new Google_Client();
            $client->setAuthConfig(env('GOOGLE_SHOPPING_FILE').'.apps.googleusercontent.com.json');
            $client->setScopes('https://www.googleapis.com/auth/content');
            $client->setAccessType('offline');
            $client->setApprovalPrompt('force');
            $client->setAccessToken($googleAccount->access_token);
            if ($client->isAccessTokenExpired()) {
                $resultData = $client->fetchAccessTokenWithRefreshToken($googleAccount->refresh_token);
                $googleAccount->access_token = json_encode($client->getAccessToken());
                $client->setAccessToken($googleAccount->access_token);
                $googleAccount->save();
            }
            try {
                echo $googleAccount->merchant_id."<br />";
                $service = new Google_Service_ShoppingContent($client);
                $products = $service->products->listProducts($googleAccount->merchant_id);
                $parameters = array();
                while (!empty($products->getResources())) {
                  foreach ($products->getResources() as $product) {
                    printf("%s %s\n", $product->getId(), $product->getTitle());
                  }
                  if (!empty($products->getNextPageToken())) {
                    break;
                  }
                  $parameters['pageToken'] = $products->nextPageToken;
                }
                echo "<br />";
            } catch(\Exception $e) {
                print_r($e->getMessage());
                echo "merchant id is not match with google account";
            }
          }
        }
    }

    public function checkAccountConnect() {
        $merchantId = session('merchant_id');
        $googleAccount = GoogleAccount::where('merchant_id', $merchantId)->first();
        if ($googleAccount) {
          $client = new Google_Client();
          $client->setAuthConfig(env('GOOGLE_SHOPPING_FILE').'.apps.googleusercontent.com.json');
          $client->setScopes('https://www.googleapis.com/auth/content');
          $client->setAccessType('offline');
          $client->setApprovalPrompt('force');
          $client->setAccessToken($googleAccount->access_token);
          if ($client->isAccessTokenExpired()) {
              $resultData = $client->fetchAccessTokenWithRefreshToken($googleAccount->refresh_token);
              $googleAccount->access_token = json_encode($client->getAccessToken());
              $client->setAccessToken($googleAccount->access_token);
              $googleAccount->save();
          }
          $service = new Google_Service_ShoppingContent($client);
          try {
              $products = $service->products->listProducts($googleAccount->merchant_id);
              try {
                  $service = new Google_Service_ShoppingContent($client);
                  $products = $service->products->listProducts($googleAccount->merchant_id);
                  $googleAccount->success_connect = true;
                  $googleAccount->save();
                  // echo "Merchant account connect success";
                  return redirect('/admin/add-merchant-account')->with('success_message','Kết nối merchant thành công!');
              } catch(\Exception $e) {
                  // echo "Merchant id is not match with google account";
                  return redirect('/admin/add-merchant-account')->with('success_message','MerchantID không khớp với tài khoản Google Shoppping Account');
              }
          } catch(\Exception $e) {
              // echo 'Merchant id is not correct. Access denied!';
              return redirect('/admin/add-merchant-account')->with('success_message','MerchantID không đúng. Access denied!');
          }
        }
    }

    public function addMerchantAccount() {
      $googleAccounts = GoogleAccount::all();
      return view('admin.google-shopping.index')->with([
          'url' 		=>		'admin-google-shopping-merchant',
          'googleAccounts'  =>      $googleAccounts
       ]);
    }

    public function connectMerchantAccount(Request $request) {
      $merchantId = $request->input('merchant_id');
      $googleAccount = GoogleAccount::where('merchant_id', $merchantId)->first();
      if (!$googleAccount || !$googleAccount->access_token) {
        $googleAccount = new GoogleAccount;
        $googleAccount->merchant_id = $merchantId;
        $googleAccount->save();
        session(['merchant_id' => $merchantId]);
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback';
        redirect()->to(filter_var($redirect_uri, FILTER_SANITIZE_URL))->send();
      } else {
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/admin/add-merchant-account';
        // redirect()->to(filter_var($redirect_uri, FILTER_SANITIZE_URL))->send();
        return redirect(filter_var($redirect_uri, FILTER_SANITIZE_URL))
                ->with('success_message', 'Kết nối merchantID thành công');
      }
    }

    public function editMerchantAccount($id) {
        $googleAccount = GoogleAccount::find($id);
        return view('admin.google-shopping.edit')->with([
            'url' 		    =>		'admin-google-shopping-merchant-edit',
            'merchantId'    =>      $googleAccount ? $googleAccount->merchant_id : '',
            'id'            =>      $id
         ]);
    }

    public function updateMerchantAccount(Request $request, $id) {
        $merchantId = $request->input('merchant_id');
        $googleAccount = GoogleAccount::find($id);
        if ($googleAccount) {
            if ($googleAccount->merchant_id != $merchantId) {
                $googleAccount->merchant_id = $merchantId;
                $googleAccount->save();
                session(['merchant_id' => $merchantId]);
                $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback';
                redirect()->to(filter_var($redirect_uri, FILTER_SANITIZE_URL))->send();
            } else {
                $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/admin/edit-merchant-account/'.$id;
                // redirect()->to(filter_var($redirect_uri, FILTER_SANITIZE_URL))->send();
                return redirect(filter_var($redirect_uri, FILTER_SANITIZE_URL))
                        ->with('success_message', 'Cập nhật merchantID thành công');
            }
        }
    }

    public function deleteMerchantAccount($id) {
        $googleAccount = GoogleAccount::find($id);
        $googleAccount->delete();
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/admin/add-merchant-account';
        // redirect()->to(filter_var($redirect_uri, FILTER_SANITIZE_URL))->send();
        return redirect(filter_var($redirect_uri, FILTER_SANITIZE_URL))
                ->with('success_message', 'Xóa merchantID thành công');
    }

    public function handleOauth2Callback() {
      $client = new Google_Client();
      $client->setAuthConfigFile(env('GOOGLE_SHOPPING_FILE').'.apps.googleusercontent.com.json');
      $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback');
      $client->setAccessType("offline");
      $client->setApprovalPrompt('force');
      $client->setScopes('https://www.googleapis.com/auth/content');
      if (!isset($_GET['code'])) {
        $auth_url = $client->createAuthUrl();
        redirect()->to(filter_var($auth_url, FILTER_SANITIZE_URL))->send();
      } else {
        $returnData = $client->authenticate($_GET['code']);
        $merchantId = session('merchant_id');
        $googleAccount = GoogleAccount::where('merchant_id', $merchantId)->first();
        if ($googleAccount) {
          $googleAccount->refresh_token = json_encode($client->getRefreshToken());
          $googleAccount->access_token = json_encode($client->getAccessToken());
          $googleAccount->save();
          $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/check-merchant-account';
          redirect()->to(filter_var($redirect_uri, FILTER_SANITIZE_URL))->send();
        }
      }
    }
}
