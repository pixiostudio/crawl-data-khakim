<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopifyStore;
use App\ShopifyProduct;
use App\SyncProductJob;

use App\GoogleAccount;
use Google_Client;
use Google_Service_ShoppingContent;
use Google_Service_ShoppingContent_Product;
use Google_Service_ShoppingContent_ProductShipping;
use Google_Service_ShoppingContent_Price;
use Google_Service_ShoppingContent_ProductShippingWeight;

use App\Helpers\Pagination;

class ShopifyStoreController extends Controller
{
    public function addShopifyStore() {
        $shopifyStores = ShopifyStore::all();
        return view('admin.shopify.index')->with([
            'url'           =>      'admin-shopify-store',
            'shopifyStores' =>      $shopifyStores
        ]);
    }

    public function addShopifyStoreDb(Request $request) {
        $shopifyStore = new ShopifyStore;
        $shopifyStore->url = $request->input('url');
        $shopifyStore->url_admin = $request->input('admin_url');
        $shopifyStore->target_country = $request->input('target_country');
        $shopifyStore->api_key = $request->input('api_key');
        $shopifyStore->api_secret_key = $request->input('api_secret_key');
        $shopifyStore->merchant_id =  $request->input('merchant_id');
        $shopifyStore->currency =  $request->input('currency');
        $shopifyStore->age_group =  $request->input('ageGroup');
        $shopifyStore->category =  $request->input('productCategory');
        $shopifyStore->gender =  $request->input('gender');
        $shopifyStore->page = 0;
        $shopifyStore->save();
        return redirect('/admin/edit-shopify-store/'.$shopifyStore->id)
                ->with('success_message', 'Lưu thông tin Shopify Store thành công! ');
    }

    public function editShopifyStore(Request $request, $id) {
        $shopifyStore = ShopifyStore::find($id);
        $products = ShopifyProduct::where('shopify_store_url',$shopifyStore->url);

        $page = ($request->query('page')) ? $request->query('page') : 1;
        // pagination
        $limitPerPage = 10;
        $numberOfProducts = $products->count();
        $totalPage = (int)($numberOfProducts / $limitPerPage) + (($numberOfProducts % $limitPerPage) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $products = ShopifyProduct::where('shopify_store_url',$shopifyStore->url)
                    ->skip($limitPerPage*($page-1))->take($limitPerPage)->get();
        return view('admin.shopify.edit')->with([
            'url'           =>      'admin-shopify-store-edit',
            'shopifyStore'  =>      $shopifyStore,
            'products'      =>      $products,
            'totalPage'     =>      $totalPage,
            'previousPage'  =>      $previousPage,
            'nextPage'      =>      $nextPage,
            'currPage'      =>      $page,
            'listPages'     =>      $listPages
        ]);
    }

    public function updateShopifyStore(Request $request, $id) {
        $shopifyStore = ShopifyStore::find($id);
        if ($shopifyStore) {
            $shopifyStore->url = $request->input('url');
            $shopifyStore->url_admin = $request->input('admin_url');
            $shopifyStore->api_key = $request->input('api_key');
            $shopifyStore->api_secret_key = $request->input('api_secret_key');
            $shopifyStore->merchant_id =  $request->input('merchant_id');
            $shopifyStore->currency =  $request->input('currency');
            $shopifyStore->target_country =  $request->input('target_country');
            $shopifyStore->category =  $request->input('productCategory');
            $shopifyStore->age_group =  $request->input('ageGroup');
            $shopifyStore->gender =  $request->input('gender');
            $shopifyStore->page = 0;
            $shopifyStore->save();
            return redirect('/admin/edit-shopify-store/'.$id)
                    ->with('success_message', 'Cập nhật Shopify Store thành công!');
        }
    }

    public function deleteShopifyStore($id) {
        $shopifyStore = ShopifyStore::find($id);
        if ($shopifyStore) {
            $shopifyStore->delete();
            return redirect('/admin/add-shopify-store')
                    ->with('success_message', 'Xóa Shopify Store thành công!');
        }
    }

    private function createProductFeed($shopifyStore, $offerId, $title, $description,
                                   $link, $imageLink, $productPrice, $weight, $color, $size) {

        $product = new Google_Service_ShoppingContent_Product();
        $product->setOfferId($offerId);
        $product->setTitle($title);
        $product->setDescription($description);
        $product->setLink($link);
        $product->setImageLink($imageLink);
        $product->setContentLanguage('en');
        $targetCountry = $shopifyStore->target_country ? $shopifyStore->target_country : 'US';
        $product->setTargetCountry($targetCountry);
        $product->setChannel('online');
        $product->setAvailability('in stock');
        $product->setCondition('new');
        $product->setGoogleProductCategory($shopifyStore->category);
        // $product->setGtin(substr(time(),0,6));
        $price = new Google_Service_ShoppingContent_Price();
        $price->setValue($productPrice);
        $price->setCurrency($shopifyStore->currency);
        $product->setPrice($price);
        $product->setAgeGroup($shopifyStore->age_group);
        $product->setGender($shopifyStore->gender);
        $product->setColor($color);
        $product->setIdentifierExists('false');
        $product->setSizes([$size]);
        // $shippingPrice = new Google_Service_ShoppingContent_Price();
        // $shippingPrice->setValue('0.99');
        // $shippingPrice->setCurrency('USD');
        // $shipping = new Google_Service_ShoppingContent_ProductShipping();
        // $shipping->setPrice($shippingPrice);
        // $shipping->setCountry('US');
        // $shipping->setService('Standard shipping');
        // $product->setShipping(array($shipping));
        $shippingWeight =
             new Google_Service_ShoppingContent_ProductShippingWeight();
        $shippingWeight->setValue($weight);
        $shippingWeight->setUnit('grams');
        $product->setShippingWeight($shippingWeight);
        // var_dump($product);
        return $product;
   }

    private function sync($products) {
        foreach ($products as $product) {
            $merchantId = $shopifyStore->merchant_id; // Replace this with your Merchant Center ID.
            // Create a product resource and insert it
            $productData = json_decode($product->json_data);
            $title = $productData->title;
            $description = $productData->body_html;
            $link = $shopifyUrl.$productData->handle;
            $variants = $productData->variants[0]; // Default get the 1st variant
            $imageLink = $variants ? $variants->featured_image->src : '';
            $productPrice = $variants->price;
            $weight = $variants->grams;
            $color = $variants->option2;
            $size = $variants->option3;
            // $size = $variants->option3;
            $client = new Google_Client();
            $client->setAuthConfig(env('GOOGLE_SHOPPING_FILE').'.apps.googleusercontent.com.json');
            $client->setScopes('https://www.googleapis.com/auth/content');
            $client->setAccessType('offline');
            $client->setApprovalPrompt('force');
            $client->setAccessToken($googleAccount->access_token);
            if ($client->isAccessTokenExpired()) {
                $resultData = $client->fetchAccessTokenWithRefreshToken($googleAccount->refresh_token);
                $googleAccount->access_token = json_encode($client->getAccessToken());
                $client->setAccessToken($googleAccount->access_token);
                $googleAccount->save();
            }

            // Tao feed moi
            if ($product->check_updated == 0) {
                $offerId = $productData->id.'-spyets';

            } else { // Update feed cu
                $offerId = $product->product_feed_id;
            }
            $productResource = $this->createProductFeed($shopifyStore,
                $offerId,
                $title,
                $description,
                $link,
                $imageLink,
                $productPrice,
                $weight,
                $color,
                $size
            );
            $service = new Google_Service_ShoppingContent($client);
            $response = $service->products->insert($googleAccount->merchant_id,$productResource);
            $product->re_sync = 0;
            $product->product_feed_id = $offerId;
            $product->save();
            print_r($response);

        }
    }

    public function syncProducts(Request $request, $id) {
        $shopifyStore = ShopifyStore::find($id);
        $selectedProducts = $request->input('selectedProducts');

        $shopifyUrl = $shopifyStore->url;
        $googleAccount = GoogleAccount::where('merchant_id', $shopifyStore->merchant_id)->first();
        $products = ShopifyProduct::where('shopify_store_url',$shopifyUrl)
                                    ->whereIn('id', $selectedProducts)->get();
        $this->sync($products);
    }

    public function syncAllProducts(Request $request, $id) {
        $shopifyStore = ShopifyStore::find($id);
        $shopifyUrl = $shopifyStore->url;
        $matchThese = [
            'shopify_store_url' => $shopifyUrl,
            're_sync' => 1
        ];
        $products = ShopifyProduct::where($matchThese);
        $limitPerPage = 200;
        $numberOfProducts = $products->count();
        $totalPage = (int)($numberOfProducts / $limitPerPage) + (($numberOfProducts % $limitPerPage) !== 0);

        $syncJobs = SyncProductJob::where('shopify_store_url', $shopifyUrl)
                                    ->first();
        if ($syncJobs) {
            // ran out of products to sync to Google Feed
            if ($syncJobs->page + 1 > $totalPage) {
                $syncJob->delete();
            }
        } else {
            $syncJobs = new SyncProductJob;
            $syncJobs->shopify_store_url = $shopifyUrl;
            $syncJobs->save();
        }
        return response()->json([
            'data' => true
        ]);
    }
}
