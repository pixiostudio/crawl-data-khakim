<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\GoogleAccount;
use App\ShopifyProduct;
use App\ShopifyStore;
use App\SyncProductJob;

use Google_Client;
use Google_Service_ShoppingContent;
use Google_Service_ShoppingContent_Product;
use Google_Service_ShoppingContent_ProductShipping;
use Google_Service_ShoppingContent_Price;
use Google_Service_ShoppingContent_ProductShippingWeight;

class CreateFeedShopifyStoreByStoreUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateFeedShopifyStoreByStoreUrl:createFeedShopifyStoreByStoreUrl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Feed Shopify Store using SyncProductJob';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     private function createProductFeed($shopifyStore, $offerId, $title, $description,
                                    $link, $imageLink, $productPrice, $weight, $color,$size) {

         $product = new Google_Service_ShoppingContent_Product();
         $product->setOfferId($offerId);
         $product->setTitle($title);
         $product->setDescription($description);
         $product->setLink($link);
         $product->setImageLink($imageLink);
         $product->setContentLanguage('en');
         // SON PIXIO
         // sua lai country lay ra tu DB da luu
         $targetCountry = $shopifyStore->target_country ? $shopifyStore->target_country : 'US';
         $product->setTargetCountry($targetCountry);
         $product->setChannel('online');
         $product->setAvailability('in stock');
         $product->setCondition('new');
         $product->setGoogleProductCategory($shopifyStore->category);
         // $product->setGtin(substr(time(),0,6));
         $price = new Google_Service_ShoppingContent_Price();
         $price->setValue($productPrice);
         $price->setCurrency($shopifyStore->currency);
         $product->setPrice($price);
         $product->setAgeGroup($shopifyStore->age_group);
         $product->setGender($shopifyStore->gender);
         $product->setColor($color);
         $product->setIdentifierExists('false');
         $product->setSizes([$size]);
         // $shippingPrice = new Google_Service_ShoppingContent_Price();
         // $shippingPrice->setValue('0.99');
         // $shippingPrice->setCurrency('USD');
         // $shipping = new Google_Service_ShoppingContent_ProductShipping();
         // $shipping->setPrice($shippingPrice);
         // $shipping->setCountry('US');
         // $shipping->setService('Standard shipping');
         // $product->setShipping(array($shipping));
         $shippingWeight =
              new Google_Service_ShoppingContent_ProductShippingWeight();
         $shippingWeight->setValue($weight);
         $shippingWeight->setUnit('grams');
         $product->setShippingWeight($shippingWeight);
         // var_dump($product);
         return $product;
    }

    public function handle()
    {
        //
        $syncJobs = SyncProductJob::all();
        foreach ($syncJobs as $syncJob) {
            $shopifyUrl = $syncJob->shopify_store_url;
            $shopifyStore = ShopifyStore::where('url', $shopifyUrl)->first();

            if ($shopifyStore) {

                $googleAccount = GoogleAccount::where('merchant_id', $shopifyStore->merchant_id)->first();
                // Get every 200 products/time
                // chi lay ra nhung product can sync
                // condion re_sync
                $matchThese = [
                    'shopify_store_url' => $shopifyUrl,
                    're_sync' => 1
                ];
                $products = ShopifyProduct::where($matchThese)->take(200)->get();
                foreach ($products as $product) {
                    $merchantId = $shopifyStore->merchant_id; // Replace this with your Merchant Center ID.
                    // Create a product resource and insert it
                    $productData = json_decode($product->json_data);
                    $title = $productData->title;
                    $description = $productData->body_html;
                    $link = $shopifyUrl.$productData->handle;

                    $variants = count($productData->variants) > 0 ? $productData->variants[0] : null; // Default get the 1st variant
                    $images = count($productData->images) > 0 ? $productData->images[0] : null;
                    $imageLink = ($variants && $variants->featured_image) ? $variants->featured_image->src : '';
                    $imageLink = (!$imageLink && $images) ? $images->src : '';

                    $productPrice = $variants->price;
                    $weight = $variants->grams;
                    $color = $variants->option2;
                    $size = $variants->option3;
                    // $size = $variants->option3;
                    $client = new Google_Client();
                    $client->setAuthConfig(env('GOOGLE_SHOPPING_FILE').".apps.googleusercontent.com.json");
                    $client->setScopes('https://www.googleapis.com/auth/content');
                    $client->setAccessType('offline');
                    $client->setApprovalPrompt('force');
                    $client->setAccessToken($googleAccount->access_token);
                    if ($client->isAccessTokenExpired()) {
                        $resultData = $client->fetchAccessTokenWithRefreshToken($googleAccount->refresh_token);
                        $googleAccount->access_token = json_encode($client->getAccessToken());
                        $client->setAccessToken($googleAccount->access_token);
                        $googleAccount->save();
                    }

                    // Tao feed moi
                    if ($product->check_updated == 0) {
                        $offerId = $productData->id.'-spyets';

                    } else { // Update feed cu
                        $offerId = $product->product_feed_id;
                    }
                    $productResource = $this->createProductFeed($shopifyStore,
                    $offerId,
                    $title,
                    $description,
                    $link,
                    $imageLink,
                    $productPrice,
                    $weight,
                    $color,
                    $size
                    );
                    $service = new Google_Service_ShoppingContent($client);
                    $response = $service->products->insert($googleAccount->merchant_id,$productResource);
                    $product->re_sync = 0;
                    $product->product_feed_id = $offerId;
                    $product->save();
                    print_r($response);

                }
                // Update page sync jobs
                $syncJob->page = $syncJob->page + 1;
                $syncJob->save();
            }

        }
    }
}
