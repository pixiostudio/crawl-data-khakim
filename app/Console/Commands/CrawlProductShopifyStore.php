<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ShopifyStore;
use App\ShopifyProduct;

class CrawlProductShopifyStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CrawlProductShopifyStore:crawlProductShopifyStore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl Product Shopify Store';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shopifyStores = ShopifyStore::all();
        foreach($shopifyStores as $shopifyStore) {
          $context = stream_context_create(
              array(
                  "http" => array(
                      "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
                  )
              )
          );
          $count = 1;
          // $maxPage = 0;
      while ($count /*&& $maxPage <= 1*/) {
            $page = $shopifyStore->page + 1;
            // $maxPage++;
            $count = 0;
            $json = file_get_contents($shopifyStore->url.'products.json?page='.$page, false, $context);
            $products = json_decode($json)->products;
            foreach ($products as $product) {
              $shopifyProduct = ShopifyProduct::where('shopify_id', $product->id)->first();
              if (!$shopifyProduct) {
                  $shopifyProduct = new ShopifyProduct;
                  $shopifyProduct->shopify_id = $product->id;
                  $shopifyProduct->json_data = json_encode($product);
                  $shopifyProduct->shopify_store_url = $shopifyStore->url;
                  $shopifyProduct->save();
                  $count++;
              } else if ($shopifyProduct->json_data != json_encode($product)) {
                  $shopifyProduct->json_data = json_encode($product);
                  $shopifyProduct->check_updated = 1;
                  $shopifyProduct->re_sync = 1;
              }
              $shopifyProduct->save();
            }
            if ($count > 0) {
              $shopifyStore->page = $page;
              $shopifyStore->save();
              $page++;
            } else {
                $shopifyStore->page = 0;
                $shopifyStore->save();
                break;
            }
            sleep(rand(4, 7));
          }
        }
    }
}
