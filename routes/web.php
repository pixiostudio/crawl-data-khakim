<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function() {
    return redirect('/admin');
});

Route::get('/check-merchant-account', 'GoogleAccountController@checkAccountConnect');
Route::get('/admin/add-merchant-account', 'GoogleAccountController@addMerchantAccount');
Route::post('/admin/add-merchant-account', 'GoogleAccountController@connectMerchantAccount');
Route::get('/admin/edit-merchant-account/{id}', 'GoogleAccountController@editMerchantAccount');
Route::post('/admin/update-merchant-account/{id}', 'GoogleAccountController@updateMerchantAccount');
Route::get('/admin/delete-merchant-account/{id}', 'GoogleAccountController@deleteMerchantAccount');
Route::get('/oauth2callback', 'GoogleAccountController@handleOauth2Callback');

Route::get('/admin/add-shopify-store', 'ShopifyStoreController@addShopifyStore');
Route::post('/admin/add-shopify-store', 'ShopifyStoreController@addShopifyStoreDb');
Route::get('/admin/edit-shopify-store/{id}', 'ShopifyStoreController@editShopifyStore');
Route::post('/admin/update-shopify-store/{id}', 'ShopifyStoreController@updateShopifyStore');
Route::get('/admin/delete-shopify-store/{id}', 'ShopifyStoreController@deleteShopifyStore');
Route::post('/admin/sync-products/{id}', 'ShopifyStoreController@syncProducts');
Route::get('/admin/sync-all-products/{id}', 'ShopifyStoreController@syncAllProducts');

Route::post('/user-register', 'UserController@register');
Route::post('/user-login', 'UserController@login');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'UserController@login');
