$(document).ready(function() {

    $('.chosen-select').chosen({width: "100%"});
    $('.summernote').summernote({

        height:300,
        fontSizes: ['8', '9', '10', '11', '12', '14', '18','20','24','28','30','32','36','40'],
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            // ['fontname', ['fontname']],
            // ['style', ['style']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            // ['height', ['height']],
            ['insert', ['table', 'link','image', 'doc', 'video', 'picture']], // image and doc are customized buttons
          ],
        callbacks: {
            onImageUpload: function(image) {
                uploadImage(image[0]);
            },
            onMediaDelete : function ($target, $editable) {
                // console.log();   // get image url
                deleteImage($target.attr('src'));

            },
            onPaste: function (e) {
                 var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                 e.preventDefault();
                 setTimeout(function () {
                   document.execCommand('insertText', false, bufferText);
                 }, 10);
               }
        }
    });

    function deleteImage(url) {
      var data = new FormData();
      data.append("url", url);
      var token = $('input[name=_token]').val();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
      });
      $.ajax({
          url: '/admin-image/delete-img',
          cache: false,
          contentType: false,
          processData: false,
          data: data,
          type: "post",

          success: function(data) {
            console.log(data);
          },
          error: function(data) {
            console.log(data);
          }
      });
    }

    function uploadImage(image) {
      var data = new FormData();
      data.append("image", image);
      var token = $('input[name=_token]').val();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
      });
      $.ajax({
          url: '/admin-image/create-img-url',
          cache: false,
          contentType: false,
          processData: false,
          data: data,
          type: "post",
          success: function(data) {

              var url = data['url'];
              var image = $('<img>').attr({src: url, width: '100%'});
              $('.summernote').summernote("insertNode", image[0]);
          },
          error: function(data) {
              console.log(data);
          }
      });
    }
    $('.dropdown-toggle').dropdown();
});
function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}

function xoa_space(str) {
    // Gộp nhiều dấu space thành 1 space
    str = str.replace(/\s+/g, ' ');
    // loại bỏ toàn bộ dấu space (nếu có) ở 2 đầu của xâu
    str.trim();
    return str;
}

function converStrtToLink(str) {
    str = xoa_dau(str);
    str = str.replace(/[^0-9a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]/gi, '');
    str = xoa_space(str);
    str = str.toLowerCase(str);

    str = str.replace(/\s/g,'-');
    return str;
}

function convertStrToCode(str) {
  var res = '';
  str = str.toUpperCase(str);
  var strArr = str.split('-');
  for (var i=0; i<strArr.length; i++) {
    res += strArr[i].charAt(0);
  }
  var date = Date.now().toString();
  date = date.substr(-3);

  return res+date;
}

$('input[name=name]').keyup(function(e) {
    var name = $(this).val();

    name = converStrtToLink(name);

    //Tour code
    tourCode = convertStrToCode(name);

    var link = window.location.href;

    link = 'tour/'+name;

    if (name != '') {
        $('input[name=url]').val(link);
        $('input[name=code]').val(tourCode);
        $('input[name=code-tour]').val(tourCode);
    }
    else {
        $('input[name=url]').val('');
        $('input[name=code]').val('');
        $('input[name=code-tour]').val('');
    }
});

$('input[name=title]').keyup(function(e) {
    var name = $(this).val();

    name = converStrtToLink(name);

    var link = window.location.href;

    link = '/'+$('input[name=slug]').val()+'/'+name;

    if (name != '') {
        $('input[name=url]').val(link);
    }
    else {
        $('input[name=url]').val('');
    }
});
