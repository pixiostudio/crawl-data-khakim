@extends('layouts.app')

@section('section-body')
<section id="section-body">
    <div class="about-containter row">
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
        <div class="col-lg-10 col-md-10 col-sm-10 row" style="text-align: center; padding-top: 50px; padding-bottom: 50px;">
          <img src="https://image.flaticon.com/icons/svg/1178/1178479.svg" style="width: 60px;"/>
          <h3 style="line-height: 30px;">
            Có vẻ như bạn đang đi lạc. <br/> Vui lòng chat với Triptour để được hỗ trợ nhé!
          </h3>
        </div>
    </div>
</section>
@endsection
