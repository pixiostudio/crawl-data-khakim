@extends('layouts.admin')

@section('header')
    <link href="{{ asset('frontend/css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{ asset('frontend/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{ asset('frontend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cấu hình Shopify</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin/add-shopify-store">Shopify</a>
            </li>
            <li class="active">
                <strong>Cấu hình Shopify</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cập nhật thông tin Store</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin/update-shopify-store/{{$shopifyStore->id}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <div class="form-group"><label class="col-lg-2 control-label">Store URL</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="url" class="form-control" required="" autofocus="" value="{{$shopifyStore ? $shopifyStore->url : ''}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Store Admin URL</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="admin_url" class="form-control" required="" value="{{$shopifyStore ? $shopifyStore->url_admin : ''}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">API Key</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="api_key" class="form-control" required="" value="{{$shopifyStore ? $shopifyStore->api_key : ''}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">API Secret Key</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="api_secret_key" class="form-control" required="" value="{{$shopifyStore ? $shopifyStore->api_secret_key : ''}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">MerchantID</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="merchant_id" class="form-control" required="" value="{{$shopifyStore ? $shopifyStore->merchant_id : ''}}">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group"><label class="col-lg-2 control-label">Currency</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="currency" class="form-control" required="" value="{{$shopifyStore ? $shopifyStore->currency : ''}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Target Country</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="target_country" class="form-control" required="" value="{{$shopifyStore ? $shopifyStore->target_country : ''}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Product Category</label>
                            <div class="col-lg-10">
                                <select id="product-category" data-placeholder="Chọn product category..." class="chosen-select" tabindex="4"; name="productCategory" required="">
                                    <option {{$shopifyStore->category == 'Apparel & Accessories > Clothing' ? 'selected' : ''}} value="Apparel & Accessories > Clothing">Apparel & Accessories > Clothing</option>
                                    <option {{$shopifyStore->category == 'Apparel & Accessories > Shoes' ? 'selected' : ''}} value="Apparel & Accessories > Shoes">Apparel & Accessories > Shoes</option>
                                    <option {{$shopifyStore->category == 'Apparel & Accessories > Clothing Accessories > Sunglasses">Apparel & Accessories > Clothing Accessories > Sunglasses' ? 'selected' : ''}} value="Apparel & Accessories > Clothing Accessories > Sunglasses">Apparel & Accessories > Clothing Accessories > Sunglasses</option>
                                    <option {{$shopifyStore->category == 'Apparel & Accessories > Handbags, Wallets & Cases > Handbags' ? 'selected' : ''}} value="Apparel & Accessories > Handbags, Wallets & Cases > Handbags">Apparel & Accessories > Handbags, Wallets & Cases > Handbags</option>
                                    <option {{$shopifyStore->category == 'Apparel & Accessories > Jewelry > Watches' ? 'selected' : ''}} value="Apparel & Accessories > Jewelry > Watches">Apparel & Accessories > Jewelry > Watches</option>
                                    <option {{$shopifyStore->category == 'Media > Books' ? 'selected' : ''}} value="Media > Books">Media > Books</option>
                                    <option {{$shopifyStore->category == 'Media > DVDs & Videos' ? 'selected' : ''}} value="Media > DVDs & Videos">Media > DVDs & Videos</option>
                                    <option {{$shopifyStore->category == 'Media > Music & Sound Recordings' ? 'selected' : ''}} value="Media > Music & Sound Recordings">Media > Music & Sound Recordings</option>
                                    <option {{$shopifyStore->category == 'Software > Video Game Software' ? 'selected' : ''}} value="Software > Video Game Software">Software > Video Game Software</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Age group</label>
                            <div class="col-lg-10">
                                <select id="age-group" data-placeholder="Chọn age group..." class="chosen-select" tabindex="4"; name="ageGroup" required="">
                                    <option {{$shopifyStore->age_group == 'adult' ? 'selected' : '' }} value="adult">adult (Typically teens or older)</option>
                                    <option {{$shopifyStore->age_group == 'kids' ? 'selected' : '' }} value="kids">kids (Between 5-13 years old)</option>
                                    <option {{$shopifyStore->age_group == 'toddler' ? 'selected' : '' }} value="toddler">toddler (Between 1-5 years old)</option>
                                    <option {{$shopifyStore->age_group == 'infant' ? 'selected' : '' }} value="infant">infant (Between 3-12 months old)</option>
                                    <option {{$shopifyStore->age_group == 'newborn' ? 'selected' : '' }} value="newborn">newborn (Up to 3 months old)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Gender</label>
                            <div class="col-lg-10">
                                <select id="gender" data-placeholder="Chọn gender..." class="chosen-select" tabindex="4"; name="gender" required="">
                                    <option {{$shopifyStore->gender == 'male' ? 'selected' : '' }} value="male">male</option>
                                    <option {{$shopifyStore->gender == 'female' ? 'selected' : '' }} value="female">female</option>
                                    <option {{$shopifyStore->gender == 'unisex' ? 'selected' : '' }} value="unisex">unisex</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Lưu</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Danh sách products</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <!-- <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Action <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a id="sync-all-feed" class="font-bold">Sync All Products to Google Feed</a></li>
                        </ul>
                    </div> -->
                    <div class="table-responsive">
                        <table class="table dataTables">
                            <thead>
                            <tr>
                                <th>#</th>
                                <!-- <th><input type="checkbox" name="checkbox[]" id="checkAll" /></th> -->
                                <th width="20%">Thumbnail</th>
                                <th width="30%">Tên</th>
                                <th>Giá</th>
                                <th>Tình trạng</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(count($products) > 0)
                                    @php
                                        $count = 1;
                                    @endphp
                                    @foreach ($products as $product)
                                        @php
                                            $productData = json_decode($product->json_data);
                                            $title = $productData->title;
                                            $variants = count($productData->variants) > 0 ? $productData->variants[0] : null; // Default get the 1st variant
                                            $images = count($productData->images) > 0 ? $productData->images[0] : null;
                                            $imageLink = ($variants && $variants->featured_image) ? $variants->featured_image->src : '';
                                            $imageLink = (!$imageLink && $images) ? $images->src : '';
                                            $productPrice = $variants->price;
                                        @endphp
                                        <tr>
                                            <td>{{$count++}}</td>
                                            <!-- <td><input type="checkbox" class="checkboxes" name="checkbox[]" value="{{$product->id}}" /></td> -->
                                            <td><img src="{{$imageLink}}" width="100" /></td>
                                            <td>{{$title}}</td>
                                            <td>{{$productPrice}} {{$shopifyStore->currency}}</td>
                                            <td>
                                                @if ($product->re_sync == 1)
                                                    <span class="label label-warning">Pending</span>
                                                @elseif ($product->re_sync == 0 && $product->product_feed_id)
                                                    <span class="label label-info">Active</span>
                                                @elseif ($product->re_sync == 0 && !$product->product_feed_id)
                                                    <span class="label label-danger">Not Active</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                            </tbody>
                        </table>
                        @php
                            $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
                            $currUrl = $fullUrl[0];

                        @endphp
                        <div class="pagination">
                          <a href="{{$currUrl}}?page={{$previousPage}}">&laquo;</a>
                          @foreach ($listPages as $page)
                            <a class="{{($page==$currPage) ? 'active':''}}" href="{{$currUrl}}?page={{$page}}">{{$page}}</a>
                          @endforeach
                          <a href="{{$currUrl}}?page={{$nextPage}}">&raquo;</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <!-- Chosen -->
    <script src="{{ asset('frontend/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ asset('frontend/js/plugins/toastr/toastr.min.js') }}"></script>
    <!-- Sweet alert -->
    <script src="{{ asset('frontend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.chosen-select').chosen({width: "100%"});
            $('#checkAll').on('click', function() {
                $('.checkboxes').prop('checked', $(this).prop('checked'));
            });
            $('#sync-feed').on('click', function() {
                var checkedProductIds = [];
                $.each($('.checkboxes:checked'), function() {
                    checkedProductIds.push($(this).val());
                });
                if (checkedProductIds.length == 0) {
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            timeOut: 2000
                        };
                        toastr.warning('Chưa có product nào được chọn được sync', 'Google Shopping Admin');
                    }, 300);
                }
                swal({
                            title: "Vui lòng chờ trong giây lát",
                            text: "Bạn có muốn sync những products này tới Google Feed? Nhấn OK để tiếp tục...",
                            type: "success",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                        }, function() {
                            var token = $('input[name=_token]').val();
                            $.ajaxSetup({
                              headers: {
                                  'X-CSRF-TOKEN': token
                              }
                            });
                            $.ajax({
                                url     :           '/admin/sync-products/{{$shopifyStore->id}}',
                                type    :           'post',
                                data    :           {selectedProducts: checkedProductIds},

                            })
                            .done(function(data) {
                                swal("Hoàn thành!", "Products đã được sync!", "success");
                            })
                            .fail(function(data) {
                                swal("Oops", "Có vẻ đang có lỗi ở phía server. Vui lòng quay lại sau!", "error");
                            })
                    });
            })
            $('#sync-all-feed').on('click', function() {
                swal({
                            title: "Bạn có muốn sync những products này tới Google Feed?",
                            // text: "Hình ảnh sau khi xóa sẽ không hồi phục lại được.",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Có",
                            cancelButtonText: "Không",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                        function (isConfirm) {
                            if (isConfirm) {
                                swal("Syncing..", "Products đang được sync sang Google Feed...", "success");
                                $.ajax({
                                    url     :           '/admin/sync-all-products/{{$shopifyStore->id}}',
                                    type    :           'get',

                                })
                                .done(function(data) {
                                    console.log(data);
                                    // swal("Hoàn thành!", "Products đã được sync!", "success");
                                })
                                .fail(function(data) {
                                    console.log(data);
                                    // swal("Oops", "Có vẻ đang có lỗi ở phía server. Vui lòng quay lại sau!", "error");
                                });
                            }
                        });
            });
        })
    </script>
@endsection
