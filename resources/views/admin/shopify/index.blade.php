@extends('layouts.admin')

@section('header')
    <link href="{{ asset('frontend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{ asset('frontend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cấu hình Shopify</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin/add-shopify-store">Shopify</a>
            </li>
            <li class="active">
                <strong>Cấu hình Shopify</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nhập thông tin Store</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                        <l  ink href="{{ asset('frontend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin/add-shopify-store" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <div class="form-group"><label class="col-lg-2 control-label">Store URL</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="url" class="form-control" required="" autofocus="">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Store Admin URL</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="admin_url" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">API Key</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="api_key" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">API Secret Key</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="api_secret_key" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">MerchantID</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="merchant_id" class="form-control" required="">
                            </div>
                        </div>

                        <hr />
                        <div class="form-group"><label class="col-lg-2 control-label">Currency</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="currency" class="form-control" required="" value="USD">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Target Country</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="target_country" class="form-control" required="" value="US">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Product Category</label>
                            <div class="col-lg-10">
                                <select id="product-category" data-placeholder="Chọn product category..." class="chosen-select" tabindex="4"; name="productCategory" required="">
                                    <option selected value="Apparel & Accessories > Clothing">Apparel & Accessories > Clothing</option>
                                    <option value="Apparel & Accessories > Shoes">Apparel & Accessories > Shoes</option>
                                    <option value="Apparel & Accessories > Clothing Accessories > Sunglasses">Apparel & Accessories > Clothing Accessories > Sunglasses</option>
                                    <option value="Apparel & Accessories > Handbags, Wallets & Cases > Handbags">Apparel & Accessories > Handbags, Wallets & Cases > Handbags</option>
                                    <option value="Apparel & Accessories > Jewelry > Watches">Apparel & Accessories > Jewelry > Watches</option>
                                    <option value="Media > Books">Media > Books</option>
                                    <option value="Media > DVDs & Videos">Media > DVDs & Videos</option>
                                    <option value="Media > Music & Sound Recordings">Media > Music & Sound Recordings</option>
                                    <option value="Software > Video Game Software">Software > Video Game Software</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Age group</label>
                            <div class="col-lg-10">
                                <select id="age-group" data-placeholder="Chọn age group..." class="chosen-select" tabindex="4"; name="ageGroup" required="">
                                    <option selected value="adult">adult (Typically teens or older)</option>
                                    <option value="kids">kids (Between 5-13 years old)</option>
                                    <option value="toddler">toddler (Between 1-5 years old)</option>
                                    <option value="infant">infant (Between 3-12 months old)</option>
                                    <option value="newborn">newborn (Up to 3 months old)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Gender</label>
                            <div class="col-lg-10">
                                <select id="gender" data-placeholder="Chọn gender..." class="chosen-select" tabindex="4"; name="gender" required="">
                                    <option selected value="male">male</option>
                                    <option value="female">female</option>
                                    <option value="unisex">unisex</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Lưu</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Danh sách Shopify Store</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table dataTables">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Store URL</th>
                                <th>MerchantID</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(count($shopifyStores) > 0)
                                    @php
                                        $count = 1;
                                    @endphp
                                    @foreach ($shopifyStores as $shopifyStore)
                                    <tr>
                                        <td>{{$count++}}</td>
                                        <td>{{$shopifyStore->url}}</td>
                                        <td>{{$shopifyStore->merchant_id}}</td>
                                        <td>
                                            <a style="margin: 5px;" href="/admin/edit-shopify-store/{{$shopifyStore->id}}" class="btn btn-warning btn-custom">Cập nhật</a>
                                            <button class="btn btn-danger btn-custom btn-delete" data-id="{{$shopifyStore->id}}">Xóa</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script src="{{ asset('frontend/js/plugins/dataTables/datatables.min.js') }}"></script>
    <!-- Chosen -->
    <script src="{{ asset('frontend/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <!-- Sweet alert -->
    <script src="{{ asset('frontend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.chosen-select').chosen({width: "100%"});
            $('.dataTables').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons:[],
                stateSave: true //save the state before reload
            });

        });

    </script>

    <script>
        $('.dataTables').on('click', '.btn-delete', function () {
            var id = $(this).data('id');
            swal({
                        title: "Bạn có chắc chắn xóa không?",
                        text: "Shopify Store sau khi xóa sẽ không hồi phục lại được.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Có",
                        cancelButtonText: "Không",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Đã xóa!", "Shopify Store đã được xóa.", "success");
                            window.location.href="/admin/delete-shopify-store/"+id;
                        } else {
                            swal("Đã hủy", "Shopify Store đã được giữ lại", "error");
                        }
                    });
        });
    </script>
@endsection
