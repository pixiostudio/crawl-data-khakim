@extends('layouts.admin')

@section('header')
    <link href="{{ asset('frontend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{ asset('frontend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cấu hình Google Shoping</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin/add-merchant-account">Google Shopping</a>
            </li>
            <li class="active">
                <strong>Cấu hình Google Shopping</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nhập thông tin MerchantID</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin/add-merchant-account" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <div class="form-group"><label class="col-lg-2 control-label">MerchantID</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="merchant_id" class="form-control" required="" autofocus="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Lưu</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Danh sách Google Shopping Account</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table dataTables">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>MerchantID</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(count($googleAccounts) > 0)
                                    @php
                                        $count = 1;
                                    @endphp
                                    @foreach ($googleAccounts as $googleAccount)
                                    <tr>
                                        <td>{{$count++}}</td>
                                        <td>{{$googleAccount->merchant_id}}</td>
                                        <td>
                                            <a style="margin: 5px;" href="/admin/edit-merchant-account/{{$googleAccount->id}}" class="btn btn-warning btn-custom">Cập nhật</a>
                                            <button class="btn btn-danger btn-custom btn-delete" data-id="{{$googleAccount->id}}">Xóa</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/dataTables/datatables.min.js') }}"></script>
    <!-- Sweet alert -->
    <script src="{{ asset('frontend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons:[],
                stateSave: true //save the state before reload
            });

        });

    </script>

    <script>
        $('.dataTables').on('click', '.btn-delete', function () {
            var id = $(this).data('id');
            swal({
                        title: "Bạn có chắc chắn xóa không?",
                        text: "Google Shopping Account sau khi xóa sẽ không hồi phục lại được.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Có",
                        cancelButtonText: "Không",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Đã xóa!", "Google Shopping Account đã được xóa.", "success");
                            window.location.href="/admin/delete-merchant-account/"+id;
                        } else {
                            swal("Đã hủy", "Google Shopping Account đã được giữ lại", "error");
                        }
                    });
        });
    </script>

@endsection
